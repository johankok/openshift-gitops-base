# Basic upstream apiserver config

## etcd encryption

To add etcd encryption, include this directory as a base, and add the following kustomization:

```
patchesStrategicMerge:
- |
  apiVersion: config.openshift.io/v1
  kind: APIServer
  metadata:
    name: cluster
  spec:
    encryption:
      type: aescbc
```
