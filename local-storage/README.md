# Local Storage Operator

This application deploy the OpenShift Local Storage Operator into the
`openshift-local-storage` namespace.

You will need to add overlays that create Local Volumes yourself. [Check the
correct version of the documentation for your
cluster](https://docs.openshift.com/container-platform/4.9/storage/persistent_storage/persistent-storage-local.html)
for instruction on how to do so.
